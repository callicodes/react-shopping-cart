import React, { useState } from "react";
import "./Counter.css";

function Counter({ step = 1 }) {
  const [count, setCount] = useState(0);
  //returns an array, state and a function
  return (
    <div className='Counter'>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + step)}>+{step}</button>
    </div>
  );
}

export default Counter;

// the component re-renders everytime the button is clicked!
