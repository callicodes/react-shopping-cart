import React from "react";

function Greeter({ name, excitement, age }) {
  //destructuring name and age props
  const greet = () => {
    alert(`Hello There ${name}`); //string literal with interpolation
  };

  return (
    <>
      <h1>
        Hello, {name}
        {"!".repeat(excitement)}
      </h1>
      <p> You are {age} years old</p>
      <button onClick={greet}>Click Me</button>
    </>
  );
}

export default Greeter;
