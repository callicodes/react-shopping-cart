import React, { useState } from "react";
import "./MoodToggler.css";

function MoodToggler() {
  const [isHappy, setIsHappy] = useState(true);
  //React use State Hooks in Action, set a name then a function
  const toggleIsHappy = () => setIsHappy(!isHappy);
  const styles = { color: isHappy ? "green" : "red" };

  return (
    <h3 className='MoodToggler' style={styles} onClick={toggleIsHappy}>
      {isHappy ? ":)" : ":("}
    </h3>
  );
}

export default MoodToggler;

// the component re-renders everytime the button is clicked!
